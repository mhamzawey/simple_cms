# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...






**Controller**
- To Generate Controller with menues: `rails g controller {controller_name} {view1} {view2}`
**One-to-One:**

Classroom has_one :teacher
Teacher belongs_to :Classroom


**One-to-many**

Teacher has_many :courses
Course belongs_to :teacher

has_many methods:

1. subject.pages
2. subject.pages << page
3. subject.pages = [page,page,page]
4. subject.pages.delete(page)
5. subject.pages.destroy(page)
6. subject.pages.clear
7. subject.pages.empty?
8. subject.pages.size


**Many-to-many** (habtm)

**Simple**
- Course `has_and_belongs_to_many :students`
- Student `has_and_belongs_to_many :courses`

Two Rules:
- default naming: first_table + _ + second_table

- disable the automatic id for it

*eg:* 
- `rails g migration CreatedUserPages` with two foreign keys `user_id` and `page_id` with index [`user_id`,`page_id`]

This will only have the foreign keys only (no extra information, no time stamps or anything) 

**Rich**:

This is used to have extra information (Rich Joins)

We don't have to follow Rails naming conventions

- We will have a join table like the simple one with two indexed foreign keys
- Requires a primary key coloumn(:id)
- Join table has to have its own model (not just a migration)

*eg:*
- `rails g model SectionEdit user_id:integer:index section_id:integer:index summary:string`
- in `User` model we should add:
    - `has_many :section_edits`
    - `has_many :sections, :through => section_edits`
- in `Section` model we should add:
    - `has_many :section_edits`
    - `has_many :users, :through => section_edits`
- in `SectionEdit` we should add:
    - `belongs_to :user`
    - `belongs_to :section`
    
 We can easily get `users` associated to `sections` by: `Section.find(1).users`
 
 We can easily get `sections` associated with `users` by: `User.find(1).sections`
 
 To get section edits `Users.find(1).section_edits` or `Section.find(1).section_edits`
 

**belongs_to **

- `{:optional => true}` if we need to save the object if it's not there. 



**CRUD ACTIONS**


![alt text](crud_actions.png "")

`rails g controller Subjects index show new edit delete` (those are the views associated to it)


Those are 5 of our actions, the other 3 are
- delete
- destroy
- update

But those don't have views associated to them, they are actions which do the form processing


**Resourceful Routes**

- `resources :subjects`
- `resources :pages`
- `resources :sections`

Those shortcuts create standard routes, which map to the `CRUD` actions using `HTTP` verbs.

**member:** expects an id (works with single resource) eg: (index)

**collection:** doesn't expect an id (works on the resource as a whole, not an existing member) eg: (new and create)


**Strong Parameters:**

The problem appeared with mass assignments, to prevent that, we have to permit the needed parameters

eg: `params.require(:subject).permit(:name, :position, :visible)`


**Flash Hash:**

- Stores message in session data
- Clears old messages after every request
- Used for messages needed after a redirect

` flash[:notice] = 'Subject was successfully updated.'` or ` notice: 'Subject was successfully updated.'`

`notice` and `error` are the most common in rails apps


**Text Helpers:**

- `word_wrap`: When you have very long text (either stored in a variable or retrieved from the DB). 
We wrap that text in order not to take more than a line length. `<%=word_wrap(text, :line_width => 30) %>`
- `simple_format`: takes text and changes it to HTML format. `<%=simple_format(text) %>`
- `truncate`:  When we have a really long section of text. Allows us to break that text to a certain length 
`<%=truncate(text, :length => 28) =>`
- `pluralize`: Useful when we have dynamic text being returned, and we don't know how many of something we're going to have ahead of time
eg: if we have product and we don't know if it's going to return one product or multiple. Do we name it Poducts or Product?
`<% [0,1,2].each do |n| %>`
`<%= pluralize(n, 'product') %> found. <br />`
`<% end %>`


**Number Helpers**
- number_to_currency `number_to_currency(number, :precision => 0, :unit => "kr", :format => "%n %u")` %n is number %u is unit
- number_to_percentage `number_to_percentage(number, :separator => ',' , :pericision => 1)`
- number_with_precision(old rails) / number_to_rounded `number_with_precision(number, :precision => 6)`
- number_with_delimiter(old rails) / number_to_delimited  `number_with_delimiter(number, :delimiter => ' '`
- number_to_human `number_to_human(number, :precision => 5)` 123456789 => 123.46 Million
- number_to_human_size `number_to_himan_size(number, :precision =>2)` work with file sizes, 1234567 => 1.2 MB
- number_to_phone `number_to_phone(number, :area_code =>true, :delimiter => ' ',:country_code =>1 , :extension => '321' `

`:delimiter` default ","
`:separator` default "."
`:precision` default "2-3"

**DateTime Helpers:**

- second  /  seconds`
- minute  /  minutes
- hour    /  hours  
- day     /  days
- week    /  weeks
- month   /  months
- year    /  years

- beginning_of_day / end_of_day
- beginning_of_week / end_of_week
- beginning_of_month / end_of_month
- beginning_of_year / end_of_year
- yesterday / tomorrow
- last_week / next_week
- last_mongth / next_month
- last_year / next_year

*Calculation examples:* 

- `Time.Now + 30.days - 23.minutes` 
- `30.days.go` Past
- `30.days.from_now` Future
- `Time.now.last_year.end_of_month.beginning_of_day`

*Out Outputs:*
- datetime.strftime( format_string )

eg: `Time.now.strftime ("%B %d, %Y %H:%M")` => `July 17, 2016 18:14`

*Default Formats:*

- `:db`
- `"number`
- `:time`
- `:short`
- `:long`
- `:long_ordinal`


**Production**

to prepare assets, do the following commands

`export RAILS_ENV=production`
`bundle exec rails assets:precompile`


**stylesheets**
- They are stored inside `app/assets/stylesheets`
- We have two manifests `admin.css` && `aplication.css`  to add any required stules, `*= require "file_name"`
- To add those manifests to productio, go to `config/initializers/assets.rb` and add the following line:
`Rails.application.config.assets.precompile += %w( admin.css )`

**JavaScript**
- They are stored inside `app/assets/javascripts`
- file extentions are either `js ` or `coffee`
- Add it to `aplication.js` manifest to add any required stules, `//= require "file_name"`
- To add those manifests to productio, go to `config/initializers/assets.rb` and add the following line:
`Rails.application.config.assets.precompile += %w( admin.css , other_manifests )`
- We can add JS directly by the following: `<%= javascript_tag do %> alert('#{text}); <%end>` to Sanitize this we will need to use `escape_javascript()` for short `j()`
- Sanitized => `<%= javascript_tag do %> alert('#{j(text)}); <%end>`

**Images**
- They are stored inside `app/assets/images`
- `<%= image_tag('logo.png') %>`
- You can add it from the stylesheet as: `background: $light_brown image-url('/assets/footer_gradient.png') r`


**Forms**
- `select(object, attribute, choices, options, html_options)`
- Options: 
    - `:selected => object.attribute`
    - `:include_blank => false`
    - `:prompt => false`
    - `disabled => true`
- Range: `f.select(:position, 1..5)`
- Array: `f.select :content_type, ['text','HTML']`
- Hash: `f.select(:visible, {"visible"=>1, "Hidden"=>2})`
- Array of Arrays: `f.select(:page_id, Page.all.map {|p| [p.name, p.id]})` 

- date_select: `date_select(object, attribute, options, html_options)`
- Options: 
    - `:start_year => Time.now.year-5`
    - `:end_year => Time.now.year+5`
    - `:order => [;year, ;month, ;day]`
    - `:discard_year => false`
    - `discard_month => false`
    - `:discard_day => false`
    - `:include_blank => false`
    - `:prompt => false`
    - `:use_month_number =>false`
    - `:add_month_numbers => false`   
    - `::use_short_month => false`
    - `:date_separator => ""`
- time `time_select(object, attribute, options, html_options)`
- Options:
    - `:include_seconds =>false`
    - `:minute_step =>1`
    -  `:include_blank => false`
    - `:prompt => false`
    - `:time_separator => ":"`
    
- date time: `datetime_select(object, attribute, options, html_options)`
- Options:
    - `:datetime_separator => "-"`


**Validations:**

- `validates_presence_of :attribute_name`
- `validates_length_of :attribute_name :is ,:minimum ,:maximum ,:within ,:in`
- `validates_numericality of :equal_to, :greater_than, :less_than, :greater_than_or_equal_to, :less_than_or_equal_to, :odd, :even, :only_integer`
- `validates_inclusion_of :in`
- `validates_exclusion_of :in`
- `validates_format_of :with {regex}`
- `validates_uniqueness_of :case_sensitive, :scope`
- `validates_acceptance_of :accept` (virtual attribute)
- `validates_confirmation_of` (eg: email confirmation)
- `validates_associated :association_name` (makes sure that associations are valid when we create different related objects)
-  *Options:*
    - `:allow_nil => true` (skips validation if attribute is null)
    - `:allow_blank => true` (skips validation if attribute is blank)
    - `:if => :method` / `:unless => :method"`
    
 - *custom method*
 
```
validate :custom_method
 private
 def custom_method:
    if test_case
      errors.add(:attribute, "message")
    end
 end
 ```

**Controller Filters**
- before_action
- after_action
- around_action

- Methods should be private
- Any render or redirect before an action prevents its execution
- Specify which actions active the filter with:
    - `:only => [:method1, :method3]`
    - `except => [:method2]`
- Filters in `ApllicationController` are inherited by all controllers
- Inherited filters can be skipped.
    - `skip_before_action`
    - `skip_after_action`
    - `skip_around_action`
    
 **logging:**
 5 options
 1. `:debug`
 2. `:info`
 3. `:warn`
 4. `:error`
 5. `:fatal`
 ![alt text](log_levels.png "")

 *eg:*
 - `config/environments/development.rb` => `config.log_level =:debug`
 - `config/environments/production.rb` => `config.log_level =:info`

- clear logs: `rails clear:log`

- Cutom logs: `logger.debug("the name is #{@subject.name}")`

**Authentication**

To Use `ActiveModel` => `has_secure_password`

- `gem bcrypt` should be installed
- Table in the DB that has a string column for `password_digest`

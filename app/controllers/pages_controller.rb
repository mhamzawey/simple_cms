class PagesController < ApplicationController
  before_action :set_page, :only => [:show, :edit, :update, :destroy]
  before_action :find_subjects, :only => [:new, :edit, :update, :create]
  before_action :set_page_count, :only => [:new, :edit, :update, :create]
  layout 'admin'
  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.sorted
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    # we don't need this as there is before_action above :set_page which is defined later as Page.find(params[:id])
    # @page = Page.find(params[:id])
  end

  # GET /pages/new
  def new
    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)
    respond_to do |format|
      if @page.save
        format.html { redirect_to pages_path, notice: "Page '#{@page.name}'  was successfully created." }
        format.json { render :show, status: :created, location: @page }
      else

        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to pages_path, notice: "Page '#{@page.name}'  was successfully updated." }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: "Page '#{@page.name}'  was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.find(params[:id])
    end

    def find_subjects
      @subjects = Subject.sorted
    end

    def set_page_count
      @page_count = Page.count
      if params[:action] == 'new' || params[:action] == 'create'
        @page_count += 1
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:subject_id, :name, :permalink, :position, :visible)
    end
end

class SubjectsController < ApplicationController
  before_action :set_subject, only: [:show, :edit, :update, :destroy]

  layout 'admin'
  # GET /subjects
  # GET /subjects.json
  def index
    @subjects = Subject.sorted
  end

  # GET /subjects/1
  # GET /subjects/1.json
  def show
    #we don't need this as there is before_action above :set_subject which is defined later as Subject.find(params[:id])
    #@subject = Subject.find(params[:id])
  end

  # GET /subjects/new
  def new
    @subject = Subject.new(:name =>"Default")
    @subject_count = Subject.count + 1
  end

  # GET /subjects/1/edit
  def edit
    @subject_count = Subject.count
  end

  # POST /subjects
  # POST /subjects.json
  def create

    # Instantiate a new object using form parameters
    @subject = Subject.new(subject_params)

    respond_to do |format|
      # Save the object
      # If save succeeds, redirect to the index action
      if @subject.save
        format.html { redirect_to subjects_path, notice: "Subject '#{@subject.name}' was successfully created." }
        format.json { render :show, status: :created, location: @subject }
      # If save fails, redisplay the form so the user can fix the problem
      else
        #render('new')
        @subject_count = Subject.count + 1
        format.html { render :new }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subjects/1
  # PATCH/PUT /subjects/1.json
  def update
    #we don't need this as there is before_action above :set_subject which is defined later as Subject.find(params[:id])
    #@subject = Subject.find(params[:id])
    respond_to do |format|
      if @subject.update(subject_params) # we use the whitelisted value
        format.html { redirect_to subjects_path, notice: "Subject '#{@subject.name}' was successfully updated." }
        format.json { render :show, status: :ok, location: @subject }
      else
        @subject_count = Subject.count
        format.html { render :edit }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subjects/1
  # DELETE /subjects/1.json
  def destroy
    @subject.destroy
    respond_to do |format|
      format.html { redirect_to subjects_path, notice: "Subject '#{@subject.name}' was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject
      @subject = Subject.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subject_params
      params.require(:subject).permit(:name, :position, :visible)
    end
end

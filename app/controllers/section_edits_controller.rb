class SectionEditsController < ApplicationController
  before_action :set_section_edit, only: [:show, :edit, :update, :destroy]

  layout 'admin'

  # GET /section_edits
  # GET /section_edits.json
  def index
    @section_edits = SectionEdit.all
  end

  # GET /section_edits/1
  # GET /section_edits/1.json
  def show
  end

  # GET /section_edits/new
  def new
    @section_edit = SectionEdit.new
  end

  # GET /section_edits/1/edit
  def edit
  end

  # POST /section_edits
  # POST /section_edits.json
  def create
    @section_edit = SectionEdit.new(section_edit_params)

    respond_to do |format|
      if @section_edit.save
        format.html { redirect_to @section_edit, notice: 'Section edit was successfully created.' }
        format.json { render :show, status: :created, location: @section_edit }
      else
        format.html { render :new }
        format.json { render json: @section_edit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /section_edits/1
  # PATCH/PUT /section_edits/1.json
  def update
    respond_to do |format|
      if @section_edit.update(section_edit_params)
        format.html { redirect_to @section_edit, notice: 'Section edit was successfully updated.' }
        format.json { render :show, status: :ok, location: @section_edit }
      else
        format.html { render :edit }
        format.json { render json: @section_edit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /section_edits/1
  # DELETE /section_edits/1.json
  def destroy
    @section_edit.destroy
    respond_to do |format|
      format.html { redirect_to section_edits_url, notice: 'Section edit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_section_edit
      @section_edit = SectionEdit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def section_edit_params
      params.require(:section_edit).permit(:user_id, :section_id, :summary)
    end
end

class SectionsController < ApplicationController
  before_action :set_section, only: [:show, :edit, :update, :destroy]
  before_action :find_pages, :only => [:new, :edit, :update, :create]
  before_action :set_section_count, :only => [:new, :edit, :update, :create]

  layout 'admin'

  # GET /sections
  # GET /sections.json
  def index
    @sections = Section.sorted
  end

  # GET /sections/1
  # GET /sections/1.json
  def show
  end

  # GET /sections/new
  def new
    @section = Section.new
  end

  # GET /sections/1/edit
  def edit
    # we don't need this as there is before_action above :set_section which is defined later as Section.find(params[:id])
    # @section = Section.find(params[:id])
  end

  # POST /sections
  # POST /sections.json
  def create
    @section = Section.new(section_params)

    respond_to do |format|
      if @section.save
        format.html { redirect_to sections_path, notice: "Section '#{@section.name}' was successfully created." }
        format.json { render :show, status: :created, location: @section }
      else
        format.html { render :new }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sections/1
  # PATCH/PUT /sections/1.json
  def update
    respond_to do |format|
      if @section.update(section_params)
        format.html { redirect_to sections_path, notice: "Section '#{@section.name}' was successfully updated." }
        format.json { render :show, status: :ok, location: @section }
      else
        @section_count = Page.count
        format.html { render :edit }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sections/1
  # DELETE /sections/1.json
  def destroy
    @section.destroy
    respond_to do |format|
      format.html { redirect_to sections_url, notice: "Section '#{@section.name}' was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_section
      @section = Section.find(params[:id])
    end

    def find_pages
      @pages = Page.sorted
    end

    def set_section_count
      @section_count = Section.count
      if params[:action] == 'new' || params[:action] == 'create'
        @section_count += 1
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def section_params
      params.require(:section).permit(:page_id, :name, :position, :visible, :content_type, :content)
    end
end

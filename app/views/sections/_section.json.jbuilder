json.extract! section, :id, :page_id, :name, :position, :visible, :content_type, :content, :created_at, :updated_at
json.url section_url(section, format: :json)

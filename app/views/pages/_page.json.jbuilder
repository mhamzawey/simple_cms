json.extract! page, :id, :subject_id, :name, :permalink, :position, :visible, :created_at, :updated_at
json.url page_url(page, format: :json)

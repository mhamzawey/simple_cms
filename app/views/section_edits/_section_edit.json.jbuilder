json.extract! section_edit, :id, :user_id, :section_id, :summary, :created_at, :updated_at
json.url section_edit_url(section_edit, format: :json)

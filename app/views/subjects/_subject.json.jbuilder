json.extract! subject, :id, :name, :position, :visible, :created_at, :updated_at
json.url subject_url(subject, format: :json)

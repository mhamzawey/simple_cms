require 'test_helper'

class SectionEditsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @section_edit = section_edits(:one)
    @subject = Subject.create(:name=>"test subject", :position => 1, :visible => true )
    @page = Page.create(:subject_id => @subject.id, :name=>"test page", :visible => true )
    @user = User.create(:username => "test_user", :first_name => 'test', :last_name=> 'test')
    @section = Section.create(:name => "Test Section", :page_id => @page.id, :visible=> true)
  end

  test "should get index" do
    get section_edits_url
    assert_response :success
  end

  test "should get new" do
    get new_section_edit_url
    assert_response :success
  end

  test "should create section_edit" do
    assert_difference('SectionEdit.count') do
      post section_edits_url, params: { section_edit: { section_id: @section.id, summary: @section_edit.summary, user_id: @user.id } }
    end

    assert_redirected_to section_edit_url(SectionEdit.last)
  end

  test "should show section_edit" do
    get section_edit_url(@section_edit)
    assert_response :success
  end

  test "should get edit" do
    get edit_section_edit_url(@section_edit)
    assert_response :success
  end

  test "should update section_edit" do
    patch section_edit_url(@section_edit), params: { section_edit: { section_id: @section.id, summary: @section_edit.summary, user_id: @user.id } }
    assert_redirected_to section_edit_url(@section_edit)
  end

  test "should destroy section_edit" do
    assert_difference('SectionEdit.count', -1) do
      delete section_edit_url(@section_edit)
    end

    assert_redirected_to section_edits_url
  end
end

require "application_system_test_case"

class SectionEditsTest < ApplicationSystemTestCase
  setup do
    @section_edit = section_edits(:one)
  end

  test "visiting the index" do
    visit section_edits_url
    assert_selector "h1", text: "Section Edits"
  end

  test "creating a Section edit" do
    visit section_edits_url
    click_on "New Section Edit"

    fill_in "Section", with: @section_edit.section_id
    fill_in "Summary", with: @section_edit.summary
    fill_in "User", with: @section_edit.user_id
    click_on "Create Section edit"

    assert_text "Section edit was successfully created"
    click_on "Back"
  end

  test "updating a Section edit" do
    visit section_edits_url
    click_on "Edit", match: :first

    fill_in "Section", with: @section_edit.section_id
    fill_in "Summary", with: @section_edit.summary
    fill_in "User", with: @section_edit.user_id
    click_on "Update Section edit"

    assert_text "Section edit was successfully updated"
    click_on "Back"
  end

  test "destroying a Section edit" do
    visit section_edits_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Section edit was successfully destroyed"
  end
end

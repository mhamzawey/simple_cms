Rails.application.routes.draw do

  get 'admin_user/index'
  get 'admin_user/new'
  get 'admin_user/create'
  get 'admin_user/edit'
  get 'admin_user/update'
  get 'admin_user/destroy'
  root 'demo#index'
  get 'admin', :to => 'access#menu'
  get 'access/menu'
  get 'access/login'
  post 'access/attempt_login'
  get 'access/logout'

  resources :section_edits
  resources :sections
  resources :pages
  resources :subjects
  resources :users


end

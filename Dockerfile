FROM ruby:2.5

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs mysql-client
COPY  Gemfile /
COPY  Gemfile.lock /
RUN cd / && bundle install
RUN mkdir /app
WORKDIR /app
